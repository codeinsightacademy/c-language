/* Return type function demo*/

#include<stdio.h>

int add(int x, int y) {
	return x + y;
}

int main() {
	int z = add(5, 6);

	printf("z = %d\n", z);

	printf("Addition of 10 and 15 is %d\n", add(10, 15));

	int a = 100 + add(10, 20);
	// int a = 100 + 30
	
	printf("a = %d\n", a);

}
