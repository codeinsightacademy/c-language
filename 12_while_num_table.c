/* print table of number using while loop 
 *
 * init
 * while(cond'n) {
 *		/stmt 1
 *		/stmt 2
 *
 *		inc/dec
 * }
 *
 *
 * */

#include<stdio.h>

int main() {
	
	int i = 1;
	int num = 5;

	while(i <= 10) {
		printf("%d * %d = %d\n", num, i, num * i);
		i++;
	}

	printf("Value of i is %d\n", i);
}
