/* table of number using for loop */

#include<stdio.h>

int main() {

	int i;

	int num;

	printf("Enter a number:");
	scanf("%d", &num);

	for(i = 1; i <= 10; i++) {
		printf("%d * %d = %d\n", num, i, num * i);
	}

	printf("Value of i is %d\n", i);

}
