/* nested if statement */

#include<stdio.h>

int main() {

	float percentage = 70.15;

	if(percentage >= 35) {
		printf("Student is passed\n");

		if(percentage >= 75)
			printf("with Ist class distinction\n");
		else if(percentage >= 60)
			printf("with Ist Class\n");

	} else {
		printf("Student is failed\n");
	}

}
