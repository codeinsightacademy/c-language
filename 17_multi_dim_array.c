/* 2 d array matrix */

#include<stdio.h>

int main() {
	int arr[2][3] = {
						{11, 22, 55},
						{33, 44, 66}
					};

	//printf("arr[0][1] = %d\n", arr[0][1]);
	
	int rows = 2;
	int cols = 3;

	int i = 0;
	int j = 0;

	for(i = 0; i < rows; i++) {

		for(j = 0; j < cols; j++) {
			printf("arr[%d][%d] = %d | ", i, j, arr[i][j]);
		}

		printf("\n");
	}
}
