/* Expressions program */

#include<stdio.h>

int main() {
	int x = 10;
	int y = 25;

	int z = x + y;

	//10 + 20 = 30
	
	printf("%d + %d = %d\n", x, y, z);

}
