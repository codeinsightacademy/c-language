/* switch case break default */

#include<stdio.h>

int main() {
	
	int num = 1;

	printf("Enter a number");
	scanf("%d", &num);

	//if(num == 1)
	//	printf("One");

	//if(num == 2)
	//	printf("Two");

	switch(num) {
		case 1:
			printf("One");
			break;
		case 2:
			printf("Two");
			break;
		default:
			printf("Invalid entry");
	}

	printf("\n");

}
