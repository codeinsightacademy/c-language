/* greatest among 3 numbers */

#include<stdio.h>

int main() {
	int x = 100;
	int y = 200;
	int z = 30;

	if(x > y && x > z) {
		printf("%d is greatest\n", x);
	} else if(y > z) {
		printf("%d is greatest\n", y);
	} else {
		printf("%d is greatest\n", z);
	}
}
