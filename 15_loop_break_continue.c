/* Break and Continue in Loop */

#include<stdio.h>

int main() {

	/*int floor;
	
	for(floor = 1; floor <= 15; floor++) {
		if(floor == 13) 
			continue;
		printf("# %d Floor come\n", floor);
	}
	*/

	int mypower = 9;
	int enemy_power = 1;
	int status = 1;

	for(enemy_power = 1; enemy_power <= 10; enemy_power++) {
		if(mypower >= enemy_power) {
			printf("%d step clear\n", enemy_power);
		} else {
			status = 0;
			break;
		}
	
	}

	if(status == 1) {
		printf("You won\n");
	} else {
		printf("You lost\n");
	}


}
