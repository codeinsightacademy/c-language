/* while loop demo 
 *
 * init
 * while(cond'n) {
 *		/stmt 1
 *		/stmt 2
 *
 *		inc/dec
 * }
 *
 *
 * */

#include<stdio.h>

int main() {
	
	int i = 10;

	while(i > 0) {
		printf("%d Hello World\n", i);
		i--;
	}

	printf("Value of i is %d\n", i);
}
