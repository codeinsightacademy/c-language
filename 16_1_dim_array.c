/* 1 dimensional array */

#include<stdio.h>

int main() {
	int arr[5] = {33, 44, 55, 66, 77};


	//printf("arr[2] = %d\n", arr[2]);
	//
	
	int i = 0;

	int len = sizeof(arr)/sizeof(int);

	printf("Size of array is %d\n", len);

	for(i = 0; i < len; i++) {
		printf("arr[%d] = %d\n", i, arr[i]);
	}

}
