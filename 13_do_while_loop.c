/* do while loop demo */

#include<stdio.h>

int main() {
	int num;

	do {
			printf("Enter a number:");
			scanf("%d", &num);
			printf("Square of %d is %d\n", num, num * num);
	} while (num != 0);

	printf("Out of loop\n");
}
