/* switch case with character */

#include<stdio.h>

int main() {
	
	char color;

	printf("Enter color code (r/g/b)");
	scanf("%c", &color);

	//if(color == 'r')
	//	printf("Red");

	switch(color) {
		case 'r':
		case 'R':
				printf("Red");
				break;
		case 'g':
		case 'G':
				printf("Green");
				break;
		case 'b':
		case 'B':
				printf("Blue");
				break;
		default:
				printf("Invalid Color Code");
	}

	printf("\n");
}
